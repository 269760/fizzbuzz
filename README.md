# fizzbuzz.FizzBuzz

Eserciziono TDD "Progetto del software"

## Commenti
Ritengo l'esperienza utile, anche solo per prendere la mano con junit e git.
Non avuto particolari difficoltà a svolgererla se non nella "quarta iterazione",
dove a causa del refactoring nella iterazione precedente mi sono trovato un codice
già funzionante an che per il caso 15.

Nel complesso mi sento di valutare l'esperienza **4/5**