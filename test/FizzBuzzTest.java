import fizzbuzz.FizzBuzz;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FizzBuzzTest {

    @Test
    void fizzBuzz() {
        assertEquals("1", FizzBuzz.fizzBuzz(1));
    }

    @Test
    void fizzBuzz3() {
        assertEquals("1 2 fizz", FizzBuzz.fizzBuzz(3));
    }

    @Test
    void fizzBuzz5() {
        assertEquals("1 2 fizz 4 buzz", FizzBuzz.fizzBuzz(5));
    }

    @Test
    void fizzBuzz15() {
        String expected = "1 2 fizz 4 buzz fizz 7 8 fizz buzz 11 fizz 13 14 fizzbuzz";
        assertEquals(expected, FizzBuzz.fizzBuzz(15));
    }
}