package fizzbuzz;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Inserisci un numero: ");
        int n = in.nextInt();
        System.out.println("Risultato: " + FizzBuzz.fizzBuzz(n));
    }
}
