package fizzbuzz;

public class FizzBuzz {
    private FizzBuzz() {}
    public static String fizzBuzz(int n) {
        StringBuilder bld = new StringBuilder();
        for(int i = 1; i <= n; i++) {
            bld.append(ifModuleZeroPrint(i, 3, "fizz"));
            bld.append(ifModuleZeroPrint(i, 5, "buzz"));
            bld.append(ifModuleNotZeroPrintNumber(i, 3, 5));
            if (i != n)
                bld.append(" ");
        }
        return bld.toString();
    }

    private static String ifModuleZeroPrint(int n, int m, String toPrint) {
        return (n%m == 0) ? toPrint : "";
    }

    private static String ifModuleNotZeroPrintNumber(int n, int m1, int m2) {
        return (n%m1 != 0 && n%m2 != 0) ? String.valueOf(n) : "";
    }
}
